# front-end-interview-question

## Checklist

### 1. HTML, CSS Semantic

- Link & Script
- Variable
- Box model
- Position
- Flexbox
- Grid
- Block - Inline
- Responsive
- Transform
- Transition
- Animation
- Theming

### 2. Javascript Scope, let - const

- Import - Export
- Primitive - Reference type
- Pure function
- Spread operators, Rest parameters, Destructuring
- Array and Array function
- Arrow function
- Async/Await - Promise - Callback
- Try/catch
- Authen: JWT, Session/Cookie and Local Storage
- Canvas (nth)

### 3. Typescript Class - Abtract - OOP

- Nullable - Undefinedable - Never
- Interface - Type - Enum
- Utility Types: Partial, Omit, Pick, Exclude...
- Generic type
- Decorator


### 4. JSX - Element

- Virtual DOM and Component Rendering
- Styling for component: inline, CSS Module, 3rd party lib (Theming)
- Class-based and Functional Component
- Prop & PropTypes, Children property
- State & lifecycle, useState()
- List Rendering, key prop
- Form & Form validation
- API Calling, Axios
- Thinking in React: Break Component
- Hooks
- Lift State Up: Parent Component, Context, useReducer(), 3rd party lib
- Advanced:
- UI lib: mui, antDesign, tailwindCSS...
- State Management: Redux: Thunk, Saga, Toolkit
- Routing, nested Routing
- useEffect(), dependencies - primitive value vs reference value
- Refs and Forwarding Refs
- Error handling: Local/Global with try/catch, Error Boundaries, axios interceptor
- write reusable code	common component
  - custom Hook
  - render prop, children prop
- Performance	- Component Rerendering: 
  - State/Prop change, memo(), useMemo/useCallback
  - Code splitting, Structure project: break component, break modules
  - Virtualize Long Lists
  - lazy load
  - Profiler API, onRender Callback
- Portals, Fragment
